@echo off

rem      *************
rem      * Constants *
rem      *************

set GIT="C:\Program Files\Git\bin\git.exe"


set THIS_DIR=%~dp0
set GLFW_PKG_DIR=%THIS_DIR%\glfw\external\pkg
set GLFW_ROOT_DIR=%THIS_DIR%\..
set PROJECTS_ROOT_DIR=%THIS_DIR%\..\..

set GLFW_BUILD_TYPES=RelWithDebInfo Release



rem      ************
rem      * Defaults *
rem      ************

set hcd_arch=amd64
set build=true
set clean=false



rem      *******************
rem      * Parse Arguments *
rem      *******************

:label_parse_arguments

if [%1]==[] (
  goto label_no_further_arguments
)

set argument=%1
shift

if [%argument:~0,1%]==[/] (
  set argument=%argument:~1%
) else if [%argument:~0,2%]==[--] (
  set argument=%argument:~2%
) else (
  echo WARNING - Ignoring unexpected argument: %argument%
  goto label_parse_arguments
)


if [%argument%]==[arch] (
  goto set_arch
) else if [%argument%]==[clean] (
  set clean=true
) else if [%argument%]==[no-build] (
  set build=false
) else (
  echo WARNING - unknown option ignored: /%argument%
)

goto label_parse_arguments


:set_arch
if [%1]==[] (
  echo WARNING: arch unspecified!
) else (
  set hcd_arch=%1
  shift
)
goto label_parse_arguments


:label_no_further_arguments



rem      *********************
rem      * Determine Version *
rem      *********************

for /f "tokens=* usebackq" %%a in (`%GIT% -C %THIS_DIR%\glfw\external\pkg describe --dirty --always`) do set version=%%a

if [%version:~-6%]==[-dirty] (
  echo GLFW source is dirty, aborting...
  exit /B 1
)



rem      *****************
rem      * Load MSVC Env *
rem      *****************

set MSVC_ENV_TOOL_DIR=%PROJECTS_ROOT_DIR%\msvcEnv\tool

call %MSVC_ENV_TOOL_DIR%\msvcEnv.bat %hcd_arch%

if %ERRORLEVEL% neq 0 (
  echo Could not load msvc env for arch: %hcd_arch%
  echo Aborting...
  exit /B 1
)



rem      *******************
rem      * Build Directory *
rem      *******************

set glfw_version_dir=%GLFW_ROOT_DIR%\%version%

if exist %glfw_version_dir% (
  echo GLFW version %version% already extracted, re-using ...
) else (
  echo Extracting GLFW version %version% ...
  robocopy %GLFW_PKG_DIR% %glfw_version_dir% /e /xf .git* > nul
)



rem      *******************
rem      * Build Directory *
rem      *******************

set build_dir=%glfw_version_dir%\%hcd_arch%\%hcd_platform%

if exist %build_dir% (
  if [%clean%]==[true] (
    rmdir /s /q %build_dir%
  ) else (
    echo Build directory already exists: %build_dir%
    echo Aborting...
    exit /B 1
  )
)

if [%build%]==[false] (
  exit
)

mkdir %build_dir%



rem      *********
rem      * CMake *
rem      *********

if [%hcd_platform%]==[vs14.0] (
  if [%hcd_arch%]==[x86] (
    set cmake_generator=Visual Studio 14 2015
  ) else if [%hcd_arch%]==[amd64] (
    set cmake_generator=Visual Studio 14 2015 Win64
  ) else (
    echo Unsupported arch:
    echo On platform: %hcd_platform%-%hcd_arch%
    exit /B 1
  )
) else (
  echo Unsupported platform: %hcd_platform%
  exit /B 1
)

pushd %build_dir%

cmake -G "%cmake_generator%" %glfw_version_dir%

if %ERRORLEVEL% neq 0 (
  echo CMake failed, aborting...
  popd
  exit /B 1
)

popd



rem      *********
rem      * Build *
rem      *********

set glfw_proj=%build_dir%\src\glfw.vcxproj

for %%a in (%GLFW_BUILD_TYPES%) do (
  MSBuild %glfw_proj% ^
          /p:Configuration=%%a
)
